import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SignInComponent } from "./components/sign-in/sign-in.component";
import { RegisterComponent } from './components/register/register.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { InvalidComponent } from './components/invalid/invalid.component';
import { ActionComponent } from './components/action/action.component';
import { ProductComponent } from './components/product/product.component';
import { InventoryComponent } from './components/inventory/inventory.component';
import { BillingComponent } from './components/billing/billing.component';
import { StockComponent } from './components/stock/stock.component';




const routes: Routes = [
  {
    path: "",
    component: WelcomeComponent
  },
  {
    path: "signin",
    component: SignInComponent
  },
  {
    path: "actions",
    component: ActionComponent,
    children: [
      {
        path: "product",
        component: ProductComponent,
      },
      {
        path: "inventory",
        component: InventoryComponent
      },
      {
        path: "billing",
        component: BillingComponent
      },
      {
        path: "stock",
        component: StockComponent
      }
    ]
  },
  {
    path: "register",
    component: RegisterComponent
  },
  {
    path: "**",
    component: InvalidComponent
  }
];



@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
