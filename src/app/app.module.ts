import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationBarComponent } from './components/navigation-bar/navigation-bar.component';
import { SignInComponent } from './components/sign-in/sign-in.component';
import { RegisterComponent } from './components/register/register.component';
import { DetailComponent } from './components/detail/detail.component';
import { WelcomeComponent } from './components/welcome/welcome.component';
import { InvalidComponent } from './components/invalid/invalid.component';
import { ProductComponent } from './components/product/product.component';
import { ActionComponent } from './components/action/action.component';
import { InventoryComponent } from './components/inventory/inventory.component';
import { BillingComponent } from './components/billing/billing.component';
import { StockComponent } from './components/stock/stock.component';
import { ListproductComponent } from './components/listproduct/listproduct.component';
import { ListinventaireComponent } from './components/listinventaire/listinventaire.component';
import { DetailinvComponent } from './components/detailinv/detailinv.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    SignInComponent,
    RegisterComponent,
    DetailComponent,
    WelcomeComponent,
    InvalidComponent,
    ProductComponent,
    ActionComponent,
    InventoryComponent,
    BillingComponent,
    StockComponent,
    ListproductComponent,
    ListinventaireComponent,
    DetailinvComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
