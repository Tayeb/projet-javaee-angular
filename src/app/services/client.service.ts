import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';


export interface Product {
  name : string;
  codeBarre: number;
  minStock : number;
  prix : number;
}
@Injectable({
  providedIn: 'root'
})
export class ClientService {

  constructor(private http: HttpClient) { }

  httpHeaders = new HttpHeaders({
     'Content-Type': 'application/json'
   });

  baseURL : string = "http://localhost:8080/";

  getAllProducts(url) {
    return this.http.get(url);
  }

  getAllProductTypes(){
    return this.http.get(this.baseURL + "types");
  }

  getCaracs(id){
    return this.http.get(this.baseURL + "types/"+id+"/characteristics")
  }

  ajouterProduit(data){
    var payload = {
      codeBarre: data.codeBarre,
      minStock: data.minStock,
      name: data.name,
      prix: data.prix,
      characteristics: [
        {
          key: {
            "characteristicId": data.key
          },
          value: data.value
        }
      ],
      type: {
        typeId: data.type
      }
    }
    return this.http.post(this.baseURL + "product", JSON.stringify(payload), { headers: this.httpHeaders});
  }

  getProduct(url){
    return this.http.get(url);
  }

  getAllInventaires(){
    return this.http.get(this.baseURL + "inventaires");
  }
}
