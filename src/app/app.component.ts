import { Component , OnInit } from '@angular/core';

import { ClientService } from './services/client.service';
import { Product } from './services/client.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{


  constructor(private clientService : ClientService){
  }

  products : Array<Product>;


  getAllProducts(){
    this.clientService.getAllProducts().subscribe((data: any) => {
      this.products = data["_embedded"]["products"];
      console.log(this.products);
    });
  }

  ngOnInit() { this.getAllProducts() }

}
