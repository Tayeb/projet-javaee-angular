import { Component, OnInit } from '@angular/core';

import { ClientService } from '../../services/client.service';

import { Product } from '../../product';


@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.css']
})
export class DetailComponent {

  constructor(private clientService : ClientService) { }


  products : Array<Object> ;

  product : Object = new Product();


  listProductsOfType(event : Object){
    this.clientService.getAllProducts(event.url).subscribe((data: any) => {
      this.products = data["_embedded"].products;
    });
  }

  modifierProduit(event){
    console.log(event.target.id);
    this.clientService.getProduct(event.target.id).subscribe((data: any) => {
      this.product = data;
    });
  }

}
