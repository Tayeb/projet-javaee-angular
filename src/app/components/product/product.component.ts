import { Component, OnInit } from '@angular/core';

import { ClientService } from '../../services/client.service';
import { Product } from '../../product';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  constructor(private clientService: ClientService) { }

  productTypes : Array<string>;

  carac : Array<Object>;

  product : Product = new Product();

  ngOnInit() {
    var i = 1;
    this.clientService.getAllProductTypes().subscribe((data: any) => {
      this.productTypes = data["_embedded"]["types"].map( (elem) => {
        return {
          type: elem.name,
          id: i++
        }
      })
    });
  }

  ajouterProduit(){
    this.clientService.ajouterProduit(this.product).subscribe((data: any) => {
      console.log("retour serveur");
      console.log(data)
    });
  }

  getCarac(event){
    var result = Array.from(event.target.children).filter(option => option.selected === true);
    var i = 1;
    this.clientService.getCaracs(result[0].id).subscribe((data: any) => {
      this.carac = data["_embedded"].characteristics.map(c => {
        return {
          carac: c.characteristic,
          id: i++
        };
      });
    })
    this.product.type = result[0].id * 1;
  }

  handleCarac(event){
    this.product.key = event.target.id * 1;
    this.product.value = event.target.value * 1;
  }

}
