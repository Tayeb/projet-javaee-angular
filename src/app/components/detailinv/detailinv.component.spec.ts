import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailinvComponent } from './detailinv.component';

describe('DetailinvComponent', () => {
  let component: DetailinvComponent;
  let fixture: ComponentFixture<DetailinvComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DetailinvComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailinvComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
