import { Component } from '@angular/core';

@Component({
  selector: 'app-detailinv',
  templateUrl: './detailinv.component.html',
  styleUrls: ['./detailinv.component.css']
})
export class DetailinvComponent {

  constructor() { }

  products : Array<Object>;

  showProducts(event){
    this.products = event.products;
  }

}
