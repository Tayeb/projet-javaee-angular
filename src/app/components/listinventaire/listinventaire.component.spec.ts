import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListinventaireComponent } from './listinventaire.component';

describe('ListinventaireComponent', () => {
  let component: ListinventaireComponent;
  let fixture: ComponentFixture<ListinventaireComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListinventaireComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListinventaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
