import { Component, OnInit , Output, EventEmitter } from '@angular/core';

import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-listinventaire',
  templateUrl: './listinventaire.component.html',
  styleUrls: ['./listinventaire.component.css']
})
export class ListinventaireComponent implements OnInit {

  constructor(private clientService : ClientService) { }



  @Output() onChooseInventaire : EventEmitter<null> = new EventEmitter<null>();

  inventaires : Array<Object>;
  products : Array<Object>;

  ngOnInit() {
    this.clientService.getAllInventaires().subscribe((data: any) => {
      this.inventaires = data["_embedded"]["inventaires"].map( (elem) => {
        return {  date: elem.date,
                  url: elem["_links"].self.href,
                  surface: elem.surface,
                  etat: elem.etat,
                  products: elem.products}
      })
    });
  }

  showProducts(event, i){
    this.onChooseInventaire.emit({ products : i.products});
  }

}
