import { Component } from '@angular/core';
import { ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.css']
})
export class ActionComponent {

  constructor(private activatedRoute : ActivatedRoute, private router : Router) { }

  showProduct(event: any){
    event.preventDefault();
    var ul = event.target.parentElement.parentElement;
    var links = ul.querySelectorAll("a");
    Array.from(links).forEach(function(link){
      link.classList.remove('active');
    });
    event.target.classList.add('active');
    this.router.navigate(['product'], { relativeTo: this.activatedRoute });
  }

  showInventory(event : any){
    event.preventDefault();
    var ul = event.target.parentElement.parentElement;
    var links = ul.querySelectorAll("a");
    Array.from(links).forEach(function(link){
      link.classList.remove('active');
    });
    event.target.classList.add('active');
    this.router.navigate(['inventory'], { relativeTo: this.activatedRoute });
  }

  showBilling(event : any){
    event.preventDefault();
    var ul = event.target.parentElement.parentElement;
    var links = ul.querySelectorAll("a");
    Array.from(links).forEach(function(link){
      link.classList.remove('active');
    });
    event.target.classList.add('active');
    this.router.navigate(['billing'], { relativeTo: this.activatedRoute });
  }

  showStock(event : any){
    event.preventDefault();
    var ul = event.target.parentElement.parentElement;
    var links = ul.querySelectorAll("a");
    Array.from(links).forEach(function(link){
      link.classList.remove('active');
    });
    event.target.classList.add('active');
    this.router.navigate(['stock'], { relativeTo: this.activatedRoute });
  }

}
