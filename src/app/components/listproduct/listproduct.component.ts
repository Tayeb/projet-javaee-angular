import { Component, OnInit , Output, EventEmitter } from '@angular/core';

import { ClientService } from '../../services/client.service';

@Component({
  selector: 'app-listproduct',
  templateUrl: './listproduct.component.html',
  styleUrls: ['./listproduct.component.css']
})
export class ListproductComponent implements OnInit {

  constructor(private clientService : ClientService) { }

  productTypes : Array<Object> ;

  @Output() onChooseType : EventEmitter<null> = new EventEmitter<null>();

  chooseType(event){
    let url = event.target.getAttribute('type');
    this.onChooseType.emit({url : url});
  }

  ngOnInit() {
    this.clientService.getAllProductTypes().subscribe((data: any) => {
      this.productTypes = data["_embedded"]["types"].map( (elem) => {
        return { name: elem.name, url: elem["_links"].products.href }
      })
    });
  }
}
